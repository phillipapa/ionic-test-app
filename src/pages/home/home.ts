import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { FirstPage } from '../first/first';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }

  btn1Clicked(){
    console.log("Button is Clicked");
  }
  btn2Clicked(){
    this.navCtrl.push(FirstPage)
  }
}
